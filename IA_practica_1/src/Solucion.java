import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Vector;

import IA.DistFS.Requests;


public class Solucion {

	//Estructura de la solucion
	protected Vector<ElementoSolucion> solucion;
	protected Vector<Integer> carga_server;

	//Informacion adicional util para el heuristico
	//carga del servidor MAS cargado
	private int max_carga_server;
	//id de del servidor con MAS carga
	private int id_max;
	//carga del servidor MENOS cargado
	private int min_carga_server;
	//id de del servidor con MENOS carga
	private int id_min;

	//Control de errores
	private int elementos;

	/**
	 * Prepara la estructura de datos que contendra la solucion.
	 * <p>
	 * Dado que cada entrada de {@link Requests} solo contiene la peticion de un archivo por un usuario
	 * podemos asumir que el numero de entradas en {@link Requests} es el tama�o de la solucion 
	 * 
	 * @param numero_peticiones Valor de tipo {@link int} que indica la cantidad de entradas que tiene la solucion.
	 * @param cantidad_servidores Valor de tipo {@link int} que indica la cantidad de servidores del problema.
	 */
	public Solucion (int numero_peticiones, int cantidad_servidores){
		solucion= new Vector<ElementoSolucion>(numero_peticiones);
		carga_server= new Vector<Integer>(cantidad_servidores);

		//Inicializacion de la carga de los servidores
		for (int i=0; i<cantidad_servidores; i++){
			carga_server.add(i, new Integer(0));
			//carga_server.set(i, new Integer(0));
		}

		//Inicializacion de las variables auxiliares para el heuristico
		max_carga_server= 0;
		id_max= 0;
		min_carga_server= 0;
		id_min= 0;

		//Inicializacion del control de errores
		elementos= 0;

	}

	/**
	 * Constructora que clona a partir de un {@link Solucion} fuente u origen
	 * @param fuente {@link Solucion} que va a ser clonada
	 */
	public Solucion(Solucion fuente) {

		//Copiamos elemento a elemento
		//Primero la solucion
		solucion= new Vector<ElementoSolucion>(fuente.solucion.size());
		for (int i=0; i< fuente.solucion.size(); i++){
			solucion.add(i, new ElementoSolucion(fuente.solucion.elementAt(i)));
		}
		//Ahora la carga de los servers
		carga_server= new Vector<Integer>(fuente.carga_server.size());
		for (int i=0; i<fuente.carga_server.size(); i++){
			carga_server.add(i, new Integer(fuente.carga_server.elementAt(i).intValue()));
		}

		//Clonado de enteros pesado pero facil de hacer xD
		max_carga_server= fuente.max_carga_server;
		id_max= fuente.id_max;

		min_carga_server= fuente.min_carga_server;
		id_min= fuente.id_min;

		elementos= fuente.elementos;
	}

	/**
	 * Modifica la carga del servidor
	 * 
	 * @param carga Valor a a�adir en la carga del servidor
	 * @param id_server servidor al que se le modifica su carga
	 * @return
	 */
	public int addCarga (int id_server, int carga){
		int estado= CodigosError.DESCONOCIDO;

		try {
			carga_server.set(id_server, carga_server.get(id_server) + new Integer(carga));
			estado= CodigosError.OK;
		} catch (Exception e) {
			estado= CodigosError.ERROR_ARRAYLIST;
		}

		if (id_server == id_max && carga <0){
			//Puede dejar de ser el server con mayor carga
			establecerMax();
		}
		else if (id_server == id_max && carga >0){
			max_carga_server+= carga;
		}
		else if(id_server == id_min && carga > 0){
			//Puede dejar de ser el server con menor carga
			establecerMin();
		}
		else if(id_server == id_min && carga <0){
			min_carga_server-= carga;
		}

		return estado;
	}

	/**
	 * Inserta un nuevo elemento de la solucion en la estructura de datos que contiene toda la solucion.
	 * @param id_request identificador de peticion
	 * @param elemento {@link ElementoSolucion} que se inserta con indice id_solucion
	 * 
	 * @return Devuelve {@link CodigosError.OK} si todo es correcto, en caso contrario devuelve un codigo de error
	 */
	public int putElementoSolucion (int id_request, ElementoSolucion elemento){
		int estado= CodigosError.DESCONOCIDO;

		try {
			solucion.add(id_request, elemento);
			Integer carga= elemento.tiempo + carga_server.elementAt(elemento.id_server);
			carga_server.set(elemento.id_server, carga);
			//Solo incrementamos la cantidad de peticiones servidas si se estan sirviendo
			if (elemento.id_server>=0){
				elementos++;
			}
			
			estado= CodigosError.OK;
		} catch (Exception e) {
			estado= CodigosError.ERROR_ARRAYLIST;
		}

		if (elementos > solucion.size()){
			estado= CodigosError.ERROR_ESTRUCTURA_SOLUCION;
		}
		return estado;
	}

	/**
	 * Resetea la estructura de datos de los servidores
	 * <p>
	 * 
	 * @param cantidad_servidores Valor de tipo {@link int} que indica la cantidad de servidores del problema.
	 */
	public void resetServidores(int cantidad_servidores) {
		for (int i=0; i<cantidad_servidores; i++){
			carga_server.set(i, 0);
		}
	}


	//Operadores de la solucion

	/**
	 * Reasigna el servidor que sirve la peticion id_request
	 * @param id_request
	 * @param id_newserver
	 * @param new_carga
	 * @return
	 */
	public int cambiarServidor(int id_request, int id_newserver, int new_carga) {
		int estado= CodigosError.DESCONOCIDO;

		try {
			ElementoSolucion sol = solucion.elementAt(id_request);
			if (addCarga(sol.id_server, -sol.tiempo)<0){
				System.err.println("Error cambio server -> addCarga #1");
			}
			sol.id_server = id_newserver;
			sol.tiempo = new_carga;
			if (addCarga(id_newserver, new_carga)<0){
				System.err.println("Error cambio server -> addCarga #2");
			}
			estado= CodigosError.OK;

		} catch (Exception e) {
			estado= CodigosError.DESCONOCIDO;
		}

		return estado;
	}

	/**
	 * Elimina de la solucion la entrada id_request
	 * 
	 * @param id_request Entrada que es eliminada de la solucion
	 * @return Se elimina la entrada id_request de la solucion y se modifica la carga de 
	 * los servidores en consonancia
	 */
	public int quitarServidor(int id_request) {
		int estado= CodigosError.DESCONOCIDO;

		try {
			int id_server= solucion.elementAt(id_request).id_server;
			//carga_server.set(id_server, carga_server.elementAt(id_server)-solucion.elementAt(id_request).tiempo);
			addCarga(id_server, -solucion.elementAt(id_request).tiempo);
			solucion.elementAt(id_request).invalidar();
			elementos--;
			estado= CodigosError.OK;
		} catch (Exception e) {
			estado= CodigosError.DESCONOCIDO;
		}

		if (elementos < 0){
			estado= CodigosError.ERROR_ESTRUCTURA_SOLUCION;
		}

		return estado;
	}

	/**
	 * Vuelve a asignar servidor a una peticion no servida.
	 * 
	 * @param id_request Identificador de la peticion a modificar
	 * @param id_newserver Identificador del servidor asignado a la peticion
	 * @param carga_newserver Tiempo de transmision del fichero
	 * @return Devuelve un valor de {@link CodigosError} dependiendo del resultado de la operacion
	 */
	public int anadirServidor(int id_request, int id_newserver, int carga_newserver) {
		int estado= CodigosError.DESCONOCIDO;

		ElementoSolucion sol= solucion.elementAt(id_request);
		if (sol.id_server < 0){
			sol.id_server= id_newserver;
			addCarga(id_newserver, carga_newserver);
			elementos++;
			estado= CodigosError.OK;
		}
		else {
			estado= CodigosError.ERROR_ESTRUCTURA_SOLUCION;
		}

		if (elementos > solucion.size()){
			estado= CodigosError.ERROR_ESTRUCTURA_SOLUCION;
		}

		return estado;
	}

	//Informacion para el heuristico
	/**
	 * Devuelve el valor de carga para el server con la menor carga.
	 * @return Devuelve un {@link int} con el valor de carga para el server que menor carga tiene
	 * en la solucion.
	 */
	public int minCarga (){
		if (id_min<0){
			return 0x7fffffff;
		}
		else{
			return min_carga_server;
		}
	}

	/**
	 * Devuelve el valor de carga para el server con mayor carga.
	 * @return Devuelve un {@link int} con el valor de carga para el server que mayor carga tiene
	 * en la solucion.
	 */
	public int maxCarga(){
		if (id_max<0){
			return 0;
		}
		else{
			return max_carga_server;
		}
	}


	//Metodos auxiliares

	/**
	 * Busca el servidor con menor carga y guarda la informacion de carga y el id del server
	 */
	public void establecerMin() {
		min_carga_server= carga_server.elementAt(0).intValue();
		id_min= 0;

		for (int i=0; i< carga_server.size(); i++){
			if (carga_server.elementAt(i).intValue() < min_carga_server){
				min_carga_server= carga_server.elementAt(i).intValue();
				id_min= i;
			}
		}

	}

	/**
	 * Busca el servidor con mayor carga y guarda la informacion de carga y el id del server
	 */
	public void establecerMax() {
		for (int i=0; i< carga_server.size(); i++){
			if (carga_server.elementAt(i).intValue() > max_carga_server){
				max_carga_server= carga_server.elementAt(i).intValue();
				id_max= i;
			}
		}
	}
	
	public int getElementos(){
		return elementos;
	}

	public void ver(PrintStream out) {

		//Printamos la carga de los servers
		for (int i=0; i<carga_server.size(); i++){
			out.println("Carga del server: " + i + " es de: " + carga_server.elementAt(i).toString());
		}

		//Printamos la solucion
		for (int i=0; i<solucion.size(); i++){
			out.println("Peticion " + i + " fichero " + solucion.elementAt(i).id_file + " usuario " + solucion.elementAt(i).id_user + " tiempo "
					+ solucion.elementAt(i).tiempo + " server " + solucion.elementAt(i).id_server);
		}

	}

	public void grabar (File f){
		FileWriter fw = null;
		try {
			fw = new FileWriter(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedWriter bw= new BufferedWriter(fw);

		try {
			bw.write("server;carga" + "\n");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//Printamos la carga de los servers
		for (int i=0; i<carga_server.size(); i++){
			try {
				bw.write(i + ";" + carga_server.elementAt(i).toString() + "\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			bw.write("peticion;fichero;usuario;tiempo;server" + "\n");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//Printamos la solucion
		for (int i=0; i<solucion.size(); i++){
			try {
				bw.write(i + ";" + solucion.elementAt(i).id_file + ";" + solucion.elementAt(i).id_user + ";"
						+ solucion.elementAt(i).tiempo + ";" + solucion.elementAt(i).id_server + "\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
