
public class Heuristica2 implements aima.search.framework.HeuristicFunction{

	/**
	 * Funcion heuristica para el caso 2 del escenario A
	 * 
	 * @param state estado actual
	 * @return devuelve el valor normalizado del estado de la carga de los servers multiplicado por el tiempo total de transmision
	 */
	public double getHeuristicValue(Object state) {
		Estado e = (Estado) state;
		Heuristica1 h = new Heuristica1();
		double valor_carga = h.getHeuristicValue(e);
		int  temps_total = 0;
		//simplemente recorremos las cargas de los servidores y obtenemos tiempo de transmisión total
		for (int i = 0; i < e.solucion.carga_server.size(); i++) {
			temps_total += e.solucion.carga_server.elementAt(i);
		}
		//Devolvemos tiempo total multiplicado por el valor de la carga 
		//(podemos pensar en cuanto peso tiene cada opción y ponerlo en funcion)
		double res = temps_total/e.peticiones.size();
		return (valor_carga+res);
	}

}
