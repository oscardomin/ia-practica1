import java.util.ArrayList;
import java.util.List;

import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;

public class ServGetSuccessorsB implements SuccessorFunction {

	@Override
	public List getSuccessors(Object state) {
		ArrayList<Successor> lista = new ArrayList<Successor>();

		Estado actual = (Estado) state;
		// System.out.println("-----------------------------------------------------------Estado actual");
		// actual.verSolucion(System.out);

		// Hay que generar una lista con todas las posibles operaciones que se
		// pueden aplicar a un estado
		for (int i = 0; i < actual.getNumeroPeticiones(); i++) {
			// Para cada operacion posible hemos de comprovar si se puede
			// aplicar
			// y generar un nuevo estado si se puede

			//En el caso de tener servidor se lo podemos quitar y no servir
			//la peticion
			if (actual.canQuitarServer(i)) {
				//podemos quitar servidor
				//pero primero clonamos el estado
				Estado sucesor;
				sucesor = actual.quitarServer(i);
				// System.out.println("------------------------------------------------------------------------------------------------Sucesor: "
				// + i + " " + j);
				// sucesor.verSolucion(System.out);
				// Y ya esta listo para a�adirlo a la lista de candidatos a
				// suceder
				// System.out.println("Cambio de server de peticion: " + i +
				// " a server: " + j);
				lista.add(new Successor("Quitar servidor a la peticion: "
						+ i, sucesor));
				// Para cada server probamos si se puede hacer el cambio
				for (int j = 0; j < actual.getNservers(); j++) {
					if (actual.canCambiarServer(i, j)) {
						// Podemos hacer el cambio de server
						// Primero clonamos a papi
						// Le aplicamos la operacion
						sucesor = actual.cambiarServer(i, j);
						// System.out.println("------------------------------------------------------------------------------------------------Sucesor: "
						// + i + " " + j);
						// sucesor.verSolucion(System.out);
						// Y ya esta listo para a�adirlo a la lista de candidatos a
						// suceder
						// System.out.println("Cambio de server de peticion: " + i +
						// " a server: " + j);
						lista.add(new Successor("Cambio de server de peticion: "
								+ i + " a server: " + j, sucesor));
					}
				}
			} else {
				//si no se puede quitar es porque no tiene
				//por lo tanto podemos a�adirlo
				for (int j = 0; j < actual.getNservers(); j++) {
					if (actual.canAnadirServer(i, j)) {
						// Podemos hacer el cambio de server
						// Primero clonamos a papi
						Estado sucesor;
						// Le aplicamos la operacion
						sucesor = actual.anadirServer(i, j);
						// System.out.println("------------------------------------------------------------------------------------------------Sucesor: "
						// + i + " " + j);
						// sucesor.verSolucion(System.out);
						// Y ya esta listo para a�adirlo a la lista de candidatos a
						// suceder
						// System.out.println("Cambio de server de peticion: " + i +
						// " a server: " + j);
						lista.add(new Successor("Anadir server a la peticion: "
								+ i + " server: " + j, sucesor));
					}
				}
			}
		}

		return lista;
	}

}
