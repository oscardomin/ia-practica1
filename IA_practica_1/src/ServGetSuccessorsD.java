import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;

public class ServGetSuccessorsD implements SuccessorFunction {

	@Override
	public List getSuccessors(Object state) {
		ArrayList<Successor> lista = new ArrayList<Successor>();
		ArrayList<Successor> res = new ArrayList<Successor>();

		Estado actual = (Estado) state;
		// System.out.println("-----------------------------------------------------------Estado actual");
		// actual.verSolucion(System.out);

		// Hay que generar una lista con todas las posibles operaciones que se
		// pueden aplicar a un estado
		Random r = new Random();
		int id_peticion = r.nextInt(actual.getNumeroPeticiones());
		// Para cada operacion posible hemos de comprovar si se puede
		// aplicar
		// y generar un nuevo estado si se puede

		//En el caso de tener servidor se lo podemos quitar y no servir
		//la peticion
		if (actual.canQuitarServer(id_peticion)) {
			//podemos quitar servidor
			//pero primero clonamos el estado
			int operador_escogido = r.nextInt(2);
			Estado sucesor;
			if (operador_escogido == 0) {
				sucesor = actual.quitarServer(id_peticion);
				// System.out.println("------------------------------------------------------------------------------------------------Sucesor: "
				// + i + " " + j);
				// sucesor.verSolucion(System.out);
				// Y ya esta listo para a�adirlo a la lista de candidatos a
				// suceder
				// System.out.println("Cambio de server de peticion: " + i +
				// " a server: " + j);
				res.add(new Successor("Quitar servidor a la peticion: "
						+ id_peticion, sucesor));
			}
			else {
				int j = 0;
				boolean puede_servir = false;
				while (j < actual.getNservers() && !puede_servir) {
					puede_servir = actual.canCambiarServer(id_peticion, j);
					if (puede_servir){
						//Podemos hacer el cambio de server
						//Primero clonamos a papi
						//Le aplicamos la operacion
						sucesor= actual.cambiarServer(id_peticion, j);
						//System.out.println("------------------------------------------------------------------------------------------------Sucesor: " + i + " " + j);
						//sucesor.verSolucion(System.out);
						//Y ya esta listo para a�adirlo a la lista de candidatos a suceder
						//System.out.println("Cambio de server de peticion: " + i + " a server: " + j);
						res.add(new Successor("Cambio de server de peticion: " + id_peticion + " a server: " + j, sucesor));
					}
					j++;
				}
			}	
		} else {
			int j = 0;
			boolean puede_servir = false;
			while (j < actual.getNservers() && !puede_servir) {
				puede_servir = actual.canCambiarServer(id_peticion, j);
				if (puede_servir){
					//Podemos hacer el cambio de server
					//Primero clonamos a papi
					Estado sucesor;
					//Le aplicamos la operacion
					sucesor= actual.anadirServer(id_peticion, j);
					//System.out.println("------------------------------------------------------------------------------------------------Sucesor: " + i + " " + j);
					//sucesor.verSolucion(System.out);
					//Y ya esta listo para a�adirlo a la lista de candidatos a suceder
					//System.out.println("Cambio de server de peticion: " + i + " a server: " + j);
					res.add(new Successor("Anadir server a la peticion: " + id_peticion + " server: " + j, sucesor));
				}
				j++;
			}
		}

		return res;
	}

}
