
public class Heuristica3 implements aima.search.framework.HeuristicFunction{

	/**
	 * Funcion que devuelve un resultado para la heuristica del caso B
	 * 
	 * @param state Estado actual
	 * @return devuelve la multiplicacion entre el valor de las peticiones no servidas normalizado y el resultado de la heuristica 2
	 */
	public double getHeuristicValue(Object state) {
		Estado e = (Estado) state;
		Heuristica2 h = new Heuristica2();
		double carga_tiempototal = h.getHeuristicValue(e);
		
		double peticiones = e.peticiones.size();
		
		//calculamos el porcentaje de no servidas
		double no_servidas = peticiones - e.solucion.getElementos();
		//devolvemos el valor normalizado de las peticiones no servidas por el valor 
		//que devuelve la heuristica 2
		return ((no_servidas/peticiones) * carga_tiempototal + carga_tiempototal);
	}

}
