/**
 * 
 * Clase auxilar que contiene los valores que forman una entrada de la solucion
 *
 */
public class ElementoSolucion {
	int id_server;
	int id_file;
	int id_user;
	int tiempo;
	
	
	public ElementoSolucion (ElementoSolucion fuente) {
		id_server= fuente.id_server;
		id_file= fuente.id_file;
		id_user= fuente.id_user;
		tiempo= fuente.tiempo;
	}

	public ElementoSolucion() {
		
	}

	public void invalidar(){
		id_server= -1;
		
	}
}
