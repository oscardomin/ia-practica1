import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;


public class ServGetSuccessorsC implements SuccessorFunction{

	public ServGetSuccessorsC() {}

	/**
	 * Funcion que devuelve una lista con los posibles estados sucesores
	 * <p>
	 * Esta clase trata el caso A, en que todas las peticiones son satisfechas por tanto
	 * no se puede dejar de servir una peticion y tampoco volver a servir una peticion.
	 * 
	 * @param state estado actual de la solucion
	 * @return devuelve la lista de los estados sucesores
	 */
	@Override
	public List getSuccessors(Object state) {
		ArrayList<Successor> lista = new ArrayList<Successor>();
		ArrayList<Successor> res = new ArrayList<Successor>();
        
		Estado actual= (Estado) state;
		//System.out.println("-----------------------------------------------------------Estado actual");
		//actual.verSolucion(System.out);
		
		Random r = new Random();
		int id_peticion = r.nextInt(actual.getNumeroPeticiones());
       //Hay que generar una lista con todas las posibles operaciones que se pueden aplicar a un estado
		//Para cada operacion posible hemos de comprovar si se puede aplicar
		//y generar un nuevo estado si se puede
		
		//Para cada server probamos si se puede hacer el cambio
		int j = 0;
		boolean puede_servir = false;
		while (j < actual.getNservers() && !puede_servir) {
			puede_servir = actual.canCambiarServer(id_peticion, j);
			if (puede_servir){
				//Podemos hacer el cambio de server
				//Primero clonamos a papi
				Estado sucesor;
				//Le aplicamos la operacion
				sucesor= actual.cambiarServer(id_peticion, j);
				//System.out.println("------------------------------------------------------------------------------------------------Sucesor: " + i + " " + j);
				//sucesor.verSolucion(System.out);
				//Y ya esta listo para a�adirlo a la lista de candidatos a suceder
				//System.out.println("Cambio de server de peticion: " + i + " a server: " + j);
				res.add(new Successor("Cambio de server de peticion: " + id_peticion + " a server: " + j, sucesor));
			}
			j++;
		}
		
		return res;
	}

}
