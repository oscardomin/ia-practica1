

public class Heuristica1 implements aima.search.framework.HeuristicFunction{

	/**
	 * Funcion heuristica para el caso 1 del escenario A
	 * 
	 * @param state estado actual
	 * @return devuelve el valor normalizado del estado de la carga de los servers
	 */
	public double getHeuristicValue(Object state) {
		Estado e = (Estado) state;
		double res;
		
		//buscamos la carga del servidor con menos carga
		//he visto que hay funciones en solucion pero nose si se usan esas aqui o pretendes que
		//esos datos ya esten actualizados
		e.solucion.establecerMin();
		int carga_min = e.solucion.minCarga();
		//Buscamos la carga del servidor con m�s carga
		e.solucion.establecerMax();
		int carga_max = e.solucion.maxCarga();
		
		//System.out.println("Carga max y carga min en Heuristica1: " + carga_max + " " + carga_min);
		double carga = carga_max - carga_min;
		//System.out.println("La diferencia de cargas es: " + carga);
		//devolvemos el valor normalizado entre 0 y 1 de la carga de servidores (mejor cuanto mas cercano a 1)
		res= carga;
		
		//System.out.println("Resultado heuristica: " + res);
		
		return res;
	}
	

}
