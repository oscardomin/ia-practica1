import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import aima.search.framework.HeuristicFunction;
import aima.search.framework.Problem;
import aima.search.framework.Search;
import aima.search.framework.SearchAgent;
import aima.search.framework.SuccessorFunction;
import aima.search.informed.HillClimbingSearch;
import aima.search.informed.SimulatedAnnealingSearch;


public class Principal {

	private static final String PETICIONES = "p";
	private static final String SERVIDORES = "s";
	private static final String REPLICACION = "b";
	private static final String FICHEROS = "f";
	private static final String USUARIOS = "u";
	private static final String SEMILLA = "r";
	
	private static final String STEPS = "z";
	private static final String STITER = "x";
	private static final String K = "c";
	private static final String LAMB = "v";

	//Sucesores para HC
	private SuccessorFunction sucesores_A = null;
	private SuccessorFunction sucesores_B = null;
	//Sucesores para SA
	private ServGetSuccessorsC sucesores_C;
	private ServGetSuccessorsD sucesores_D;
	//Busqueda con HilClimbing
	private Search searchHill = null;
	//Busqueda con SimmulatedAnnealing
	private Search searchAnnealing = null;
	//Funcion heuristica para el caso A tipo de heuristica 1
	private HeuristicFunction a1 = null;
	//Funcion heuristica para el caso A tipo de heuristica 2
	private HeuristicFunction a2 = null;
	//Funcion heuristica para el caso B
	private HeuristicFunction b = null;


	private static int servers;
	private static float ratio;
	private static int s;
	private static int num_users;
	private static int num_files;
	private static int sr;
	private static int rat_serv;

	private Problem problema_a1;
	private Problem problema_a2;
	private Problem problema_a1b;
	private Problem problema_a2b;
	private Problem problema_b1;
	private Problem problema_b2;

	private Estado inicial_a1;
	private Estado inicial_a2;
	private Estado inicial_b1;
	private Estado inicial_b2;

	private Estado sol_a1;
	private Estado sol_a2;
	private Estado sol_a1b;
	private Estado sol_a2b;
	private Estado sol_b1;
	private Estado sol_b2;
	
	private int steps;
	private int stiter;
	private int k;
	private double lamb;
	private Problem problema_a1SA;
	private Problem problema_a2SA;
	private Problem problema_a1bSA;
	private Problem problema_a2bSA;
	private Problem problema_b1SA;
	private Problem problema_b2SA;
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		File f;
		File f2;

		Principal exec= new Principal();

		//Gestor de argumentos
		Options opt= new Options();

		//Argumentos para el estado inicial
		opt= opt.addOption(PETICIONES, true, "Cantidad de peticiones");
		opt= opt.addOption(SERVIDORES, true, "Cantidad de servidores");
		opt= opt.addOption(REPLICACION, true, "Ratio de replicacion");
		opt= opt.addOption(FICHEROS, true, "Cantidad de ficheros");
		opt= opt.addOption(USUARIOS, true, "Cantidad de usuarios");
		opt= opt.addOption(SEMILLA, true, "Semilla");

		//Opciones para SimulatedAnnealing
		opt= opt.addOption(STEPS, true, "Parametro steps para Simulated Annealing");
		opt= opt.addOption(STITER, true, "Parametro stiter para Simulated Annealing");
		opt= opt.addOption(K, true, "Parametro k para Simulated Annealing");
		opt= opt.addOption(LAMB, true, "Parametro lamb para Simulated Annealing");

		//Parser de los argumentos
		CommandLineParser parser= new PosixParser();
		CommandLine cmd= null;
		try {
			cmd = parser.parse(opt, args);
		} catch (ParseException e) {
			e.printStackTrace();

		}

		//Lectura de los argumentos

		if (cmd.hasOption(PETICIONES) &&
				cmd.hasOption(SERVIDORES)&&
				cmd.hasOption(REPLICACION)&&
				cmd.hasOption(FICHEROS)&&
				cmd.hasOption(USUARIOS)&&
				cmd.hasOption(SEMILLA)&&
				cmd.hasOption(STEPS)&&
				cmd.hasOption(STITER)&&
				cmd.hasOption(K)&&
				cmd.hasOption(LAMB)){

			//Tenemos todos los argumentos para generar un estado inicial

			exec.servers= Integer.parseInt(cmd.getOptionValue(SERVIDORES));
			exec.ratio= Float.parseFloat(cmd.getOptionValue(REPLICACION));
			exec.s= Integer.parseInt(cmd.getOptionValue(SEMILLA));
			exec.num_users= Integer.parseInt(cmd.getOptionValue(USUARIOS));
			exec.num_files= Integer.parseInt(cmd.getOptionValue(FICHEROS));
			exec.sr= s;
			exec.rat_serv= Integer.parseInt(cmd.getOptionValue(PETICIONES));
			
			exec.steps= Integer.parseInt(cmd.getOptionValue(STEPS));
			exec.stiter= Integer.parseInt(cmd.getOptionValue(STITER));
			exec.k= Integer.parseInt(cmd.getOptionValue(K));
			exec.lamb= Double.parseDouble(cmd.getOptionValue(LAMB));


			//Generar estado inicial para las diferentes rutas
			exec.inicial_a1= new Estado(servers, ratio, s, num_users, num_files, sr, rat_serv);
			exec.inicial_a2= new Estado(servers, ratio, s, num_users, num_files, sr, rat_serv);
			exec.inicial_b1= new Estado(servers, ratio, s, num_users, num_files, sr, rat_serv);
			exec.inicial_b2= new Estado(servers, ratio, s, num_users, num_files, sr, rat_serv);

			//Generacion de estado inicial A1 -- Se sirven todas las peticiones, estado inicial aleatorio
			exec.inicial_a1.crearEstadoInicialAleatorio1();
			exec.inicial_a1.verSolucion(System.out);
			f = new File("./inicial_a1.csv");
			if (!f.exists()){
				try {
					f.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			exec.inicial_a1.grabar(f);

			//Generacion de estado inicial A2 -- Se sirven todas las peticiones, estado inicial "greedy"
			exec.inicial_a2.crearEstadoInicialAleatorio1();
			exec.inicial_a2.verSolucion(System.out);
			f = new File("./inicial_a2.csv");
			if (!f.exists()){
				try {
					f.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			exec.inicial_a2.grabar(f);

			//Generacion de estado inicial B1 -- NO se sirven todas las peticiones, estado inicial aleatorio
			exec.inicial_b1.crearEstadoInicialAleatorio1();
			exec.inicial_b1.verSolucion(System.out);
			f = new File("./inicial_b1.csv");
			if (!f.exists()){
				try {
					f.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			exec.inicial_b1.grabar(f);

			//Generacion de estado inicial B2 -- NO se sirven todas las peticiones, estado inicial "greedy"
			exec.inicial_b2.crearEstadoInicialAleatorio1();
			exec.inicial_b2.verSolucion(System.out);
			f = new File("./inicial_b2.csv");
			if (!f.exists()){
				try {
					f.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			exec.inicial_b2.grabar(f);


			//Inicializacion de Generadoras de sucesores
			exec.sucesores_A = new ServGetSuccessorsA();
			exec.sucesores_B= new ServGetSuccessorsB();
			exec.sucesores_C= new ServGetSuccessorsC();
			exec.sucesores_D= new ServGetSuccessorsD();

			//Inicializacion de algoritmo de busqueda
			exec.searchHill = new HillClimbingSearch();
			exec.searchAnnealing= new SimulatedAnnealingSearch(exec.steps, exec.stiter, exec.k, exec.lamb);

			//Inicializacion de heuristicas
			exec.a1= new Heuristica1();
			exec.a2= new Heuristica2();
			exec.b= new Heuristica3();

			//Inicializacion de problemas
			//Caso A heuristica 1
			exec.problema_a1 = new Problem(exec.inicial_a1, exec.sucesores_A, new ServGoalTest(), exec.a1);
			exec.problema_a2 = new Problem(exec.inicial_a2, exec.sucesores_A, new ServGoalTest(), exec.a1);
			//Caso A heuristica 2
			exec.problema_a1b = new Problem(exec.inicial_a1, exec.sucesores_A, new ServGoalTest(), exec.a2);
			exec.problema_a2b = new Problem(exec.inicial_a2, exec.sucesores_A, new ServGoalTest(), exec.a2);
			//Caso B heuristica 3
			exec.problema_b1 = new Problem(exec.inicial_b1, exec.sucesores_B, new ServGoalTest(), exec.b);
			exec.problema_b2 = new Problem(exec.inicial_b2, exec.sucesores_B, new ServGoalTest(), exec.b);
			
			//Caso A heuristica 1
			exec.problema_a1SA = new Problem(exec.inicial_a1, exec.sucesores_C, new ServGoalTest(), exec.a1);
			exec.problema_a2SA = new Problem(exec.inicial_a2, exec.sucesores_C, new ServGoalTest(), exec.a1);
			//Caso A heuristica 2
			exec.problema_a1bSA = new Problem(exec.inicial_a1, exec.sucesores_C, new ServGoalTest(), exec.a2);
			exec.problema_a2bSA = new Problem(exec.inicial_a2, exec.sucesores_C, new ServGoalTest(), exec.a2);
			//Caso B heuristica 3
			exec.problema_b1SA = new Problem(exec.inicial_b1, exec.sucesores_D, new ServGoalTest(), exec.b);
			exec.problema_b2SA = new Problem(exec.inicial_b2, exec.sucesores_D, new ServGoalTest(), exec.b);

			//Generacion de soluciones
			hill(exec);
			Annealing(exec);


		}
		else{
			System.out.println("Faltan argumentos!");
		}
	}

	private static void hill (Principal exec){
		File f2;

		//Problema a1
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_a1, exec.searchHill);
			exec.sol_a1= (Estado) exec.searchHill.getGoalState();
			//exec.sol_a1.verSolucion(System.out);
			System.out.println("Iniciando problema a1 HC");
			f2 = new File("./final_a1.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_a1.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema a2
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_a2, exec.searchHill);
			exec.sol_a2= (Estado) exec.searchHill.getGoalState();
			//exec.sol_a2.verSolucion(System.out);
			System.out.println("Iniciando problema a2 HC");
			f2 = new File("./final_a2.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_a2.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema a1b
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_a1b, exec.searchHill);
			exec.sol_a1b= (Estado) exec.searchHill.getGoalState();
			//exec.sol_a1b.verSolucion(System.out);
			System.out.println("Iniciando problema a1b HC");
			f2 = new File("./final_a1b.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_a1b.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema a2b
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_a2b, exec.searchHill);
			exec.sol_a2b= (Estado) exec.searchHill.getGoalState();
			//exec.sol_a2b.verSolucion(System.out);
			System.out.println("Iniciando problema a2b HC");
			f2 = new File("./final_a2b.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_a2b.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema b1
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_b1, exec.searchHill);
			exec.sol_b1= (Estado) exec.searchHill.getGoalState();
			//exec.sol_b1.verSolucion(System.out);
			System.out.println("Iniciando problema b1 HC");
			f2 = new File("./final_b1.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_b1.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema b2
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_b2, exec.searchHill);
			exec.sol_b2= (Estado) exec.searchHill.getGoalState();
			//exec.sol_a1b.verSolucion(System.out);
			System.out.println("Iniciando problema b2 HC");
			f2 = new File("./final_b2.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_b2.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}


	}
	
	private static void Annealing (Principal exec){
		File f2;

		//Problema a1
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_a1SA, exec.searchAnnealing);
			exec.sol_a1= (Estado) exec.searchAnnealing.getGoalState();
			//exec.sol_a1.verSolucion(System.out);
			System.out.println("Iniciando problema a1 SA");
			f2 = new File("./final_a1SA.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_a1.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema a2
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_a2SA, exec.searchAnnealing);
			exec.sol_a2= (Estado) exec.searchAnnealing.getGoalState();
			//exec.sol_a2.verSolucion(System.out);
			System.out.println("Iniciando problema a2 SA");
			f2 = new File("./final_a2SA.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_a2.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema a1b
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_a1bSA, exec.searchAnnealing);
			exec.sol_a1b= (Estado) exec.searchAnnealing.getGoalState();
			//exec.sol_a1b.verSolucion(System.out);
			System.out.println("Iniciando problema a1b SA");
			f2 = new File("./final_a1bSA.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_a1b.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema a2b
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_a2bSA, exec.searchAnnealing);
			exec.sol_a2b= (Estado) exec.searchAnnealing.getGoalState();
			//exec.sol_a2b.verSolucion(System.out);
			System.out.println("Iniciando problema a2b SA");
			f2 = new File("./final_a2bSA.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_a2b.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema b1
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_b1SA, exec.searchAnnealing);
			exec.sol_b1= (Estado) exec.searchAnnealing.getGoalState();
			//exec.sol_b1.verSolucion(System.out);
			System.out.println("Iniciando problema b1 SA");
			f2 = new File("./final_b1SA.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_b1.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//Problema b2
		try {
			SearchAgent agenthc = new SearchAgent(exec.problema_b2SA, exec.searchAnnealing);
			exec.sol_b2= (Estado) exec.searchAnnealing.getGoalState();
			//exec.sol_a1b.verSolucion(System.out);
			System.out.println("Iniciando problema b2 SA");
			f2 = new File("./final_b2SA.csv");
			if (!f2.exists()){
				try {
					f2.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			exec.sol_b2.grabar(f2);

		} catch (Exception e) {
			e.printStackTrace();
		}


	}

}
