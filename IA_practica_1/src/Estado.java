import java.io.File;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import IA.DistFS.Requests;
import IA.DistFS.Servers;
import IA.DistFS.Servers.WrongParametersException;

/**
 * 
 * Esta clase simula el comportamiento del servidor de peticiones
 *
 */
public class Estado {

	//Variables de utilidad
	private int nservers;
	private float replication_ratio;
	private int seed_servers;
	private int users;
	private int files;
	private int seed_requests;
	private int ratio_servidas;
	private int nrequests;
	public static final int ID_USER = 0;
	public static final int ID_FILE = 1;

	//Estructuras de datos que contienen el problema a resolver
	Servers servidores; 
	Requests peticiones;

	//Estructuras de datos que contienen la solucion
	Solucion solucion;
	/**
	 * Constructora que genera un problema totalmente aleatorio
	 */
	public Estado (){
		//TODO Generar valores aleatorios de Servers i de Peticiones
		inicializadoras();
	}

	/**
	 * Constructora que genera un problema determinado a partir de los valores pasados por parametros
	 * @param servers Cantidad de servidores que tiene el Sistema de datos distribuidos
	 * @param ratio Tasa de replicacion de los ficheros almacenados en los servidores
	 * @param s Semilla utilizada para la generacion de los servidores
	 * @param num_users
	 * @param num_files
	 * @param sr 
	 */
	public Estado(int servers, float ratio, int s, int num_users, int num_files, int sr, int rat_serv){
		nservers= servers;
		replication_ratio= ratio;
		seed_servers= s;

		users= num_users;
		files= num_files;
		seed_requests= sr;

		ratio_servidas= rat_serv;

		inicializadoras();

		solucion = new Solucion(nrequests, nservers);
	}

	/**
	 * Constructora que genera un estado determinado copiandolo de uno ya iniciado
	 * @param fuente estado que queremos copiar
	 */
	public Estado(Estado fuente) {
		//El objetivo es que la estructura resultante sea un clon de la fuente

		nservers= fuente.nservers;
		replication_ratio= fuente.replication_ratio;
		seed_servers= fuente.seed_servers;

		users= fuente.users;
		files= fuente.files;
		seed_requests= fuente.seed_requests;

		ratio_servidas= fuente.ratio_servidas;
		nrequests= fuente.nrequests;

		//Nos permitimos el lujo de usar el mismo objeto ya que estas estructuras
		//no se modifican en ningun momento
		servidores= fuente.servidores; 
		peticiones= fuente.peticiones;

		//La estructura de la solucion debe ser copiada ya que se va a modificar
		solucion= new Solucion(fuente.solucion);

	}


	/**
	 * Metodo que inicializa las estructuras con los valores obtenidos o generados en la constructora
	 */
	private void inicializadoras() {
		try {
			servidores= new Servers(nservers, (int) (nservers*replication_ratio), seed_servers);
		} catch (WrongParametersException e) {
			e.printStackTrace();
		}
		peticiones= new Requests(users, files, seed_requests);

		nrequests = peticiones.size();
	}

	/**
	 * Metodo que genera una soluci�n para el caso 1 en modo aleatorio
	 */
	public void crearEstadoInicialAleatorio1() {
		//para cada peticion escogemos un servidor al azar
		int id_file;
		int id_user;
		Set<Integer> candidatos_server;
		Iterator<Integer> navegador;

		int escogido;
		int carga_escogido;
		
		Random r = new Random();
		
		ElementoSolucion elem;
		
		for (int peticion=0; peticion< nrequests; peticion++){
			id_file= peticiones.getRequest(peticion)[ID_FILE];
			id_user= peticiones.getRequest(peticion)[ID_USER];
			candidatos_server= servidores.fileLocations(id_file);
			navegador= candidatos_server.iterator();

			escogido = r.nextInt(candidatos_server.size());
			
			//la idea es recorrer el iterator hasta dar con el servidor, pero tengo mis dudas 
			//de que esto lo haga bien
			for (int i = 1; i < escogido; i++) {
				navegador.next();
			}
			escogido= navegador.next().intValue();
			carga_escogido= servidores.tranmissionTime(escogido, id_user);

			elem= new ElementoSolucion();
			elem.id_file= id_file;
			elem.id_server= escogido;
			elem.id_user= id_user;
			elem.tiempo= carga_escogido;

			solucion.putElementoSolucion(peticion, elem);
		}
		solucion.establecerMax();
		solucion.establecerMin();
	}

	/**
	 * Metodo que genera una soluci�n para el caso 1 en modo eficiente
	 */
	public void crearEstadoInicialEficiente1() {
		//Para cada peticion hemos de seleccionar el server con menor tiempo
		int id_file;
		int id_user;
		Set<Integer> candidatos_server;
		Iterator<Integer> navegador;

		int escogido;
		int carga_escogido;
		int consultado;
		int carga_consultado;

		ElementoSolucion elem;

		for (int peticion=0; peticion< nrequests; peticion++){
			id_file= peticiones.getRequest(peticion)[ID_FILE];
			id_user= peticiones.getRequest(peticion)[ID_USER];
			candidatos_server= servidores.fileLocations(id_file);
			navegador= candidatos_server.iterator();

			escogido= navegador.next().intValue();
			carga_escogido= servidores.tranmissionTime(escogido, id_user);

			//System.err.println("Carga inicial es: " + escogido);

			while (navegador.hasNext()){
				consultado= navegador.next().intValue();
				carga_consultado= servidores.tranmissionTime(consultado, id_user);
				//System.err.println("Carga consultada es: " + carga_consultado);

				if (carga_consultado<carga_escogido){
					//El consultado es mas rapido que el escogido
					escogido= consultado;
					carga_escogido= carga_consultado;
				}
			}

			elem= new ElementoSolucion();
			elem.id_file= id_file;
			elem.id_server= escogido;
			elem.id_user= id_user;
			elem.tiempo= carga_escogido;

			solucion.putElementoSolucion(peticion, elem);
		}
		solucion.establecerMax();
		solucion.establecerMin();
	}

	/**
	 * Metodo que genera una soluci�n para el caso 2 en modo aleatorio
	 */
	public void crearEstadoInicialAleatorio2() {
		//para cada peticion escogemos un servidor al azar
		int id_file;
		int id_user;
		Set<Integer> candidatos_server;
		Iterator<Integer> navegador;

		int escogido;
		int carga_escogido;
		int peticiones_servidas = (int) nrequests*ratio_servidas;
		
		Random r = new Random();
		
		ElementoSolucion elem;
		
		for (int peticion=0; peticion< peticiones_servidas; peticion++){
			id_file= peticiones.getRequest(peticion)[ID_FILE];
			id_user= peticiones.getRequest(peticion)[ID_USER];
			candidatos_server= servidores.fileLocations(id_file);
			navegador= candidatos_server.iterator();

			escogido = r.nextInt(candidatos_server.size());
			
			//la idea es recorrer el iterator hasta dar con el servidor, pero tengo mis dudas 
			//de que esto lo haga bien
			for (int i = 1; i < escogido; i++) {
				navegador.next();
			}
			escogido= navegador.next().intValue();
			carga_escogido= servidores.tranmissionTime(escogido, id_user);

			elem= new ElementoSolucion();
			elem.id_file= id_file;
			elem.id_server= escogido;
			elem.id_user= id_user;
			elem.tiempo= carga_escogido;

			solucion.putElementoSolucion(peticion, elem);
		}
		solucion.establecerMax();
		solucion.establecerMin();
		for (int peticion = peticiones_servidas; peticion < nrequests; peticion++) {
			id_file= peticiones.getRequest(peticion)[ID_FILE];
			id_user= peticiones.getRequest(peticion)[ID_USER];
			
			elem= new ElementoSolucion();
			elem.id_file= id_file;
			elem.id_server= -1;
			elem.id_user= id_user;
			elem.tiempo= -1;
			
			solucion.putElementoSolucion(peticion, elem);
		}
	}

	/**
	 * Metodo que genera una soluci�n para el caso 2 en modo eficiente
	 */
	public void crearEstadoInicialEficiente2() {
		//Para cada peticion hemos de seleccionar el server con menor tiempo
		int id_file;
		int id_user;
		Set<Integer> candidatos_server;
		Iterator<Integer> navegador;

		int escogido;
		int carga_escogido;
		int consultado;
		int carga_consultado;
		int peticiones_servidas = (int) nrequests*ratio_servidas;

		ElementoSolucion elem;

		for (int peticion=0; peticion< peticiones_servidas; peticion++){
			id_file= peticiones.getRequest(peticion)[ID_FILE];
			id_user= peticiones.getRequest(peticion)[ID_USER];
			candidatos_server= servidores.fileLocations(id_file);
			navegador= candidatos_server.iterator();

			escogido= navegador.next().intValue();
			carga_escogido= servidores.tranmissionTime(escogido, id_user);

			//System.err.println("Carga inicial es: " + escogido);

			while (navegador.hasNext()){
				consultado= navegador.next().intValue();
				carga_consultado= servidores.tranmissionTime(consultado, id_user);
				//System.err.println("Carga consultada es: " + carga_consultado);

				if (carga_consultado<carga_escogido){
					//El consultado es mas rapido que el escogido
					escogido= consultado;
					carga_escogido= carga_consultado;
				}
			}

			elem= new ElementoSolucion();
			elem.id_file= id_file;
			elem.id_server= escogido;
			elem.id_user= id_user;
			elem.tiempo= carga_escogido;

			solucion.putElementoSolucion(peticion, elem);

		}
		solucion.establecerMax();
		solucion.establecerMin();
		for (int peticion = peticiones_servidas; peticion < nrequests; peticion++) {
			id_file= peticiones.getRequest(peticion)[ID_FILE];
			id_user= peticiones.getRequest(peticion)[ID_USER];
			
			elem= new ElementoSolucion();
			elem.id_file= id_file;
			elem.id_server= -1;
			elem.id_user= id_user;
			elem.tiempo= -1;
			
			solucion.putElementoSolucion(peticion, elem);
		}
	}

	public int getNservers() {
		return nservers;
	}

	public int getRatio_servidas() {
		return ratio_servidas;
	}

	public int getNumeroPeticiones (){
		return nrequests;
	}
	
	public int getServidas(){
		return solucion.getElementos();
	}

	//Operaciones de cambio de estado
	/**
	 * Comprueba si es posible realizar un cambio de servidor de la peticion n_request al servidor new_server
	 * @param n_request Peticion a la que se quiere cambiar de server
	 * @param new_server Server al que se le quiere asignar la peticion
	 * @return Devuelve <code>true</code> en caso que sea posible
	 */
	public boolean canCambiarServer (int n_request, int new_server){
		boolean res= false;
		
		if (solucion.solucion.elementAt(n_request).id_server<0 || solucion.solucion.elementAt(n_request).id_server==new_server){
			//No se esta sirviendo la peticion, es incorrecto hacer un cambio de servidor
			res= false;
			return res;
		}

		//Obtenemos la lista de servers que pueden satisfacer la peticion
		int id_file= peticiones.getRequest(n_request)[ID_FILE];
		Set<Integer> posibles= servidores.fileLocations(id_file);

		Iterator<Integer> navegador= posibles.iterator();

		//Nos pateamos todos los candidatos hasta agotarlos o encontrar el que buscamos
		while(navegador.hasNext()){
			if (navegador.next().intValue()== new_server){
				//Podemos cambiar al server indicado
				res= true;

				//No hace falta continuar ya hemos encontrado la respuesta
				return res;
			}
		}

		//si hemos llegado aqui no podemos hacer el cambio de server solicitado
		return res;
	}

	/**
	 * Cambia el servidor que satisface la petcion n_request a new_server
	 * <p>
	 * Esta operacion no comprueba si el cambio es coherente con las restricciones del problema
	 * para hacer esta comprovacion esta el metodo canCambiarServer.
	 * @param n_request Peticion a la que se le cambia el servidor que la satisface
	 * @param new_server Servidor que pasa a hacerse cargo de la peticion
	 * @return Estado resultante de hacer el cambio de servidor
	 */
	public Estado cambiarServer (int n_request, int new_server){
		//Clonamos el estado que hace la peticion
		Estado res= new Estado(this);

		//Hacemos el cambio, si no se ha comprobado si es correcto a joderse
		int user= res.peticiones.getRequest(n_request)[ID_USER];
		int carga= res.servidores.tranmissionTime(new_server, user);

		if (res.solucion.cambiarServidor(n_request, new_server, carga)<0){
			System.err.println("EXTERMINATE EXTERMINATE");
		}

		return res;
	}

	public boolean canQuitarServer(int n_request){
		boolean res= false;

		if (solucion.solucion.elementAt(n_request).id_server<0){
			//No tiene server asignado, alias no se esta sirviendo esta peticion, alias no se puede matar a un muerto
			res= false;
		}
		else{
			//Tiene servidor asignado, por tanto puede dejar de ser servida
			res= true;
		}

		return res;
	}

	public Estado quitarServer (int n_request){
		Estado res= new Estado(this);

		//Dejamos de servir la peticion
		if (res.solucion.quitarServidor(n_request)<0){
			System.err.println("Exterminar Exterminar");
		}

		return res;
	}

	public boolean canAnadirServer (int n_request, int new_server){
		boolean res= false;

		if (solucion.solucion.elementAt(n_request).id_server>0){
			//La peticion ya esta siendo servida, por tanto a (CENSORED)
			res= false;
		}
		else{
			//La peticion no esta siendo servida, por le que puede ser a�adida
			//Obtenemos la lista de servers que pueden satisfacer la peticion
			int id_file= peticiones.getRequest(n_request)[ID_FILE];
			Set<Integer> posibles= servidores.fileLocations(id_file);

			Iterator<Integer> navegador= posibles.iterator();

			//Nos pateamos todos los candidatos hasta agotarlos o encontrar el que buscamos
			while(navegador.hasNext()){
				if (navegador.next().intValue()== new_server){
					//Podemos cambiar al server indicado
					res= true;

					//No hace falta continuar ya hemos encontrado la respuesta
					return res;
				}
			}
		}

		return res;
	}

	public Estado anadirServer (int n_request, int new_server){
		Estado res= new Estado(this);

		int user= res.peticiones.getRequest(n_request)[ID_USER];
		int carga= res.servidores.tranmissionTime(new_server, user);
		res.solucion.anadirServidor(n_request, new_server, carga);

		return res;
	}

	//Visualizar solucion

	public void verSolucion (PrintStream out){
		solucion.ver(out);
	}
	
	public void grabar (File f){
		solucion.grabar(f);
	}
}
