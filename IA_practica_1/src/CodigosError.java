/**
 * 
 * Clase que contiene los codigos de error que son devueltos por cualquier funcion del programa
 *
 */
public class CodigosError {

	
	public static final int OK= 1;
	public static final int DESCONOCIDO = -1;
	public static final int ERROR_ARRAYLIST = -2;
	public static final int SERVER_YA_EXISTE = -3;
	public static final int ERROR_ESTRUCTURA_SOLUCION = -4;

}
